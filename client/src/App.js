// import './App.css';
import "./index.css";
import React, {useEffect, useState} from 'react';
import DisplayList from './Components/DisplayList.js';
import TaskForm from './Components/TaskForm.js';
import axios from 'axios';

const serverPort = "5000";
const preURL = "http://localhost:" + serverPort;

function App() {
  const [data, setData] = useState([])
  useEffect(()=>{
   UpdateData();
  }, [])
  const UpdateData = () =>
  {
    fetch(preURL + "/task/get")
    .then(res => res.json())
    .then(data => setData(data))
    .catch(error=> console.log(error))
  }

  const handleSubmit = async (formData) =>{
    try {
      const response = await axios.post(preURL + '/task/add', formData);
      console.log(response.data);
      UpdateData();
    } catch (error) {
      console.error(error);
    }
  }
  const HandleDelete = (id)=>
  {
    DeleteFromDataBase(id);
    const newData = data.filter((e) => e.id !== id);
    setData(newData);
  }
  const HandleEdit = async (id, newContent) =>
  {
    console.log("New Content, id: ", id, "content: ", newContent);
    try {
      const response = await axios.post('http://localhost:5000/task/edit', {id: id, newContent: newContent});
      console.log(response.data);
      UpdateData();
  } catch (error) {
      console.error(error);
  }

  }
  const DeleteFromDataBase = async (id)=>{
    try {
      const response = await axios.post('http://localhost:5000/task/delete', {id: id});
      console.log(response.data);
      // UpdateData();
    } 
    catch (error) {
      console.error(error);
    }
  }

  const HandleState = async (id, state) =>{
    try {
      const response = await axios.post('http://localhost:5000/task/state', {id: id, state: state});
      console.log(response.data);
      // UpdateData();
    } 
    catch (error) {
      console.error(error);
    }

  }
  return (
    <div className="App" class=" bg-gradient-to-t from-zinc-700 to-gray-600 h-[600px] w-[500px] p-1 rounded-xl drop-shadow-2xl text-white flex flex-col items-center gap-4">
      <h1 class="text-6xl font-bold ">TO DO</h1>
      <TaskForm onSubmit={handleSubmit} />
      <DisplayList data={data} Update={UpdateData} Delete={id => HandleDelete(id)} Edit={(id,newContent)=>HandleEdit(id, newContent)} State={(id,state) => HandleState(id, state)}/>
    </div>
  );
}

export default App;
