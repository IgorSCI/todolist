import React, { useState,useRef } from 'react';
import Buttons from './Buttons';
import check from '../icons/check.png';

const SingleTask = (props) =>{

    const [showImage, setShowImage] = useState(false);
    const [tick, setTick] = useState(false);
    // const [editState, setEditState] = useState(false);
    const checkedClass  = useRef('');
    const section  = useRef('');


    const handleMouseEnter = () => 
    {
      setShowImage(true);
    };
    const handleMouseLeave = () => 
    {
      setShowImage(false);
    };
        
    const handleUpdate = () =>
    {
        props.Update();
    }
    const handleEdit = (id) =>{
      props.Edit(id);
    }
    const handleTick = () =>{

      setTick(!tick);
      const new_class = " checkClass"
      if(!tick) 
      {
        section.current.className+=new_class;
        props.State(props.data.id, 1);
      }

      else if (tick) 
      {
        section.current.className = section.current.className.substring(0, section.current.className.length - new_class.length)
        props.State(props.data.id, 0);
      }
    }
    return (
        <section
          className="flex flex-row items-center w-96 h-14 text-black font-semibold text-xl shadow-xl">
          <section ref={section} className="w-[70%] max-w-[80%] rounded-l-md gap-3 h-14 bg-gray-100 flex items-center hover:bg-gray-300 "  
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
          >
            <span onClick={handleTick} class="ml-4 w-7 h-7 absolute border-[3px] line-tho border-gray-700 cursor-pointer ">
             { tick &&
               <img src={check} alt="check" class="w-8"/>
             }
            </span>
            <span className="ml-14 w-[80%] h-full flex items-center">
              <p className="truncate">{props.data.content}</p>
            </span>
          </section>
          <Buttons className="w-[20%]" id={props.data.id} Edit={(id) => handleEdit(id)} Update={handleUpdate} Delete={(id) => props.Delete(id)} />
          {showImage && props.data.image && (
            <div
              className="absolute z-50"
              style={{
                position: "fixed",
                background: `url(${props.data.image}) no-repeat center center`,
                backgroundSize: "contain",
                width: "400px",
                height: "600px",
                left: `550px`,
                top: `10px`,
              }}
            />
          )}
        </section>
      );
}
export default SingleTask