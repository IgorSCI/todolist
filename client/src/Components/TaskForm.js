import React, { useState } from 'react';
import addIcon from '../icons/add.svg';
import uploadIcon from '../icons/upload.svg';
import uploadedIcon from '../icons/uploaded2.png'
const TaskForm= (props) =>{
    
    const [content, setContent] = useState('');
    const [image, setImage] = useState('');
    const [imageIcon, setImageIcon] = useState(uploadIcon);

    const handleImage = (event) => {
      setImageIcon(uploadedIcon);
        const file = event.target.files[0];
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          setImage(reader.result);
        };
        reader.onerror = (error) => {
          console.error('Error: ', error);
        };
      };
      const handleSubmit = async (event) =>{
        event.preventDefault();
        const formData = {content, image };
        props.onSubmit(formData);
        setContent('');
        setImage('');
        setImageIcon(uploadIcon);
      }

    return(
      <section>
        <form onSubmit={handleSubmit} class="flex flex-row gap">
            <label for="custom-input-file" class="h-12 p-1 bg-zinc-500 w-12  rounded-l-md flex items-center justify-center hover:bg-zinc-600 "><img  class="w-9" src={imageIcon} alt="add"/></label>
            <span class="text-black "><input type="text" class="outline-none w-64 h-12 p-2 text-lg font-semibold text-gray-700" maxLength="42"  placeholder=" Add task..." required  value={content} onChange={(e) => setContent(e.target.value)}/> </span>
            <button type="submit" class="bg-[#FEE715] p-1 h-12 rounded-r-md hover:bg-[#e2ce1d]"><img class="w-10 h-7" src={addIcon} alt="icon"></img></button>
            <span class="hidden"><input type="file" id="custom-input-file" onChange={handleImage}/> </span>
        </form>
      </section>

    )
}
export default TaskForm
