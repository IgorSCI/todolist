import React, { useState } from 'react';
import SingleTask from './SingleTask';

const DisplayList = (props) => {

  const [editState, setEditState] = useState(false);
  const [newContent, setNewContent] = useState("");
  const [editId, SetEditId] = useState(null);
  const [tick, setTick] = useState(false);


  const handleEdit = (id) =>{
    console.log("Edit: ", id);
    setEditState(!editState);
    SetEditId(id);
  }
  const handleEditSubmit = () =>
  {
    props.Edit(editId, newContent);
    setEditState(!editState);
  }
  const handleState= (id, state) =>
  {
    console.log(id, state);
    props.State(id, state);
  }
  
  return (
    <section class="flex flex-col mb-3 gap-4  h-full scroll overflow-y-auto mb-2 custom-scroll-bar">
      { !editState && props.data.map((e) => {
        return (<SingleTask data={e} Delete={id => props.Delete(id)} Edit={id => handleEdit(id)} State={(id, state) =>handleState(id, state)}/>)
      })}
      {
        editState && 
        <section class="mt-10 ">
          <form onSubmit={handleEditSubmit}>
            <input type="text" required min="1" onChange={(e) => setNewContent(e.target.value)} placeholder='Enter a new name...' class="rounded-l-md outline-none  p-2 h-14  text-lg font-semibold text-gray-700"/>
            <input type="submit" onSubmit={handleEditSubmit} class="h-14 bg-blue-200 w-20 text-lg text-black font-semibold rounded-r-md cursor-pointer" value="Edytuj"/>
          </form>
        </section>
      }
  </section>
  );
};
export default DisplayList;
