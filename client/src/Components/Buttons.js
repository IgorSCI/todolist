import edit from "../icons/edit.png";
import remove from '../icons/bin.png';
const Buttons= (props) => {

    const handleDeleteButton = async () => {
        props.Delete(props.id);
    }
    
    const handleEditButton =() =>
    {
        props.Edit(props.id);
    }
    
return (
    <div className="h-12 flex flex-row items-center">
        <button onClick={handleDeleteButton} class="bg-red-400 w-14 h-14 flex items-center justify-center hover:bg-red-500 ">
            <img class = "w-7" src={remove} alt="delete"/>
        </button>
        <button onClick={handleEditButton} class="bg-green-300 w-14 h-14  flex items-center justify-center rounded-r-md hover:bg-green-400">
            <img class = "w-7 text-white" src={edit} alt="edit"/>
        </button>
    </div>

)
}

export default Buttons