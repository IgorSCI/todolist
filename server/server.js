const express = require('express');
const mysql = require('mysql');
const corse = require('cors')
const app = express();
const port = 5000;
const bodyParser = require('body-parser');

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(corse())
app.use(express.json());

const db = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "todolist"
})
app.get('/task/get', (req,res) => {
  const sql = "SELECT * FROM `tasks`";
  db.query(sql, (error, data)=>
  {
    if(error) return res.json(error);
    return res.json(data);
  })

})
app.post('/task/add', (req, res) => {
  const {content, image } = req.body;
  const sql = "INSERT INTO `tasks` (`content`, `image`, `date_added`, `date_updated`) VALUES (?, ?, NOW(), NOW())";
  db.query(sql, [content, image], (err, result) => {
    if (err) {
      console.error(err);
      return res.status(500).json({ error: 'Error adding task to database' });
    }
    return res.status(201).json({ id: result.insertId });
  });
});

app.post('/task/delete', (req, res) => {
  const id = req.body.id;
  const sql = "DELETE FROM `tasks` WHERE `tasks`.`id` = ?";
  db.query(sql, [id], (error, result) =>{
    if (error) {
      console.error(error);
      return res.status(500).json({ error: 'Error deleting task from database' });
    }
    return res.status(201).json({ Success: "Successfully delted task id = " + id + " from database "});

  });
});
app.post('/task/edit', (req, res)=>{
  const id = req.body.id;
  const newContent = req.body.newContent;
  // `asds ${asdawda}`
  const sql = "UPDATE `tasks` SET `tasks`.`content` = ? WHERE `tasks`.`id` = ?";
  db.query(sql, [newContent, id], (error, result) =>{
    if (error) {
      console.error(error);
      return res.status(500).json({ error: 'Error editing task from database' });
    }
    return res.status(201).json({ Success: "Successfully edited task id = " + id + " from database"});
  })
})
app.post('/task/state' , (req,res) =>{
  const status = req.body.state;
  const id = req.body.id;
  const sql = "UPDATE `tasks` SET `tasks`.`state` = ? WHERE `tasks`.`id` = ?";
  db.query(sql, [status, id], (error, result) =>{
    if (error) {
      console.error(error);
      return res.status(500).json({ error: 'Error changing state task from database' });
    }
    return res.status(201).json({ Success: "Successfully change status on task id = " + id + " from database"});
  });
});

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
